# Firmware
PRODUCT_COPY_FILES += \
    device/xiaomi/platina/firmware/abl.elf:install/firmware-update/abl.elf \
    device/xiaomi/platina/firmware/BTFM.bin:install/firmware-update/BTFM.bin \
    device/xiaomi/platina/firmware/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    device/xiaomi/platina/firmware/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    device/xiaomi/platina/firmware/devcfg.mbn:install/firmware-update/devcfg.mbn \
    device/xiaomi/platina/firmware/dspso.bin:install/firmware-update/dspso.bin \
    device/xiaomi/platina/firmware/hyp.mbn:install/firmware-update/hyp.mbn \
    device/xiaomi/platina/firmware/keymaster64.mbn:install/firmware-update/keymaster64.mbn \
    device/xiaomi/platina/firmware/logfs_ufs_8mb.bin:install/firmware-update/logfs_ufs_8mb.bin \
    device/xiaomi/platina/firmware/logo.img:install/firmware-update/logo.img \
    device/xiaomi/platina/firmware/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    device/xiaomi/platina/firmware/pmic.elf:install/firmware-update/pmic.elf \
    device/xiaomi/platina/firmware/rpm.mbn:install/firmware-update/rpm.mbn \
    device/xiaomi/platina/firmware/storsec.mbn:install/firmware-update/storsec.mbn \
    device/xiaomi/platina/firmware/tz.mbn:install/firmware-update/tz.mbn \
    device/xiaomi/platina/firmware/xbl.elf:install/firmware-update/xbl.elf
